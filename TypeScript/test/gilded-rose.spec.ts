import { expect } from 'chai';
import { GildedRose, Item } from '../app/gilded-rose';

describe('Gilded Rose', function () {

    it('should degrade the quality of normal items by 1 every day has not passed', function() {
        const gildedRose = new GildedRose([ new Item('foo', 8, 10) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(9);
    });
    
    it('should degrade the quality of normal items by 2 every day when the sell date has passed', function() {
        const gildedRose = new GildedRose([ new Item('foo', 0, 10) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(8);
    });

    it('should not degrade quality of normal items below 0', function() {
        const gildedRose = new GildedRose([ new Item('foo', 3, 0) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(0);
    });
    
    it('should degrade the quality of conjured items by 2 every day when sell by date has not passed', function() {
        const gildedRose = new GildedRose([ new Item('Conjured Mana Cake', 3, 6) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(4);
    });
    
    it('should degrade the quality of conjured items by 4 every day when sell by date has passed', function() {
        const gildedRose = new GildedRose([ new Item('Conjured Mana Cake', 0, 6) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(2);
    });

    it('should not degrade quality of conjured items below 0', function() {
        const gildedRose = new GildedRose([ new Item('Conjured Mana Cake', 3, 0) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(0);
    });

    it('should increase the quality of aged brie by 1 every day when sell by date has not passed', function() {
        const gildedRose = new GildedRose([ new Item('Aged Brie', 3, 6) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(7);
    });
    
    it('should degrade the quality of aged brie by 2 every day when sell by date has passed', function() {
        const gildedRose = new GildedRose([ new Item('Aged Brie', 0, 6) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(8);
    });

    it('should not increase quality of aged brie over 50', function() {
        const gildedRose = new GildedRose([ new Item('Aged Brie', 3, 50) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(50);
    });

    it('should not increase quality of backstage passes over 50', function() {
        const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 3, 50) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(50);
    });

    it('should give backstage passes for past events a quality of 0', function() {
        const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', -1, 50) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(0);
    });

    it('should increase the quality of backstage passes by one', function() {
        const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 20, 1) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(2);
    });

    it('should increase the quality of backstage passes by two 10 days before the event', function() {
        const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 10, 1) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(3);
    });
    
    it('should increase the quality of backstage passes by three 5 days before the event', function() {
        const gildedRose = new GildedRose([ new Item('Backstage passes to a TAFKAL80ETC concert', 5, 1) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(4);
    });
    
    it('should not decrease the quality of sulfuras', function() {
        const gildedRose = new GildedRose([ new Item('Sulfuras, Hand of Ragnaros', 5, 1) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].quality).to.equal(1);
    });

});
