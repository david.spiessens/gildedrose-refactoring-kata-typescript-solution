import { AgedBrieStrategy } from "./Strategies/AgedBrieStrategy";
import { BackstagePassStrategy } from "./Strategies/BackstagePassStrategy";
import { ConjuredItemStrategy } from "./Strategies/ConjuredItemStrategy";
import { LegendaryItemStrategy } from "./Strategies/LegendaryItemStrategy";
import { NormalItemStrategy } from "./Strategies/NormalItemStrategy";

export class Item {
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}

export class GildedRose {
    items: Array<Item>;

    constructor(items = [] as Array<Item>) {
        this.items = items;
    }

    updateQuality() {
        this.items.forEach(item => {
            switch (item.name) {
                case 'Sulfuras, Hand of Ragnaros':    
                    LegendaryItemStrategy.process(item);
                    break;
                case 'Backstage passes to a TAFKAL80ETC concert':
                    BackstagePassStrategy.process(item);
                    break;
                case 'Aged Brie':
                    AgedBrieStrategy.process(item);
                    break;
                case 'Conjured Mana Cake':
                    ConjuredItemStrategy.process(item);
                    break;
                default:
                    NormalItemStrategy.process(item);
            }
        });

        return this.items;
    }
}