import { Item } from "../gilded-rose";
import { BaseStrategy } from "./BaseStrategy";
import { StrategyInterface } from "./StrategyInterface";

export abstract class NormalItemStrategy extends BaseStrategy implements StrategyInterface {
    public static process(item: Item) {
        this.decreaseSellIn(item);
        
        this.decreaseQuality(item);

        if (this.isExpired(item)) {
            this.decreaseQuality(item);
        }
    }
}