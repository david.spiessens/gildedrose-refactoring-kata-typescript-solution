import { Item } from "../gilded-rose";
import { BaseStrategy } from "./BaseStrategy";
import { StrategyInterface } from "./StrategyInterface";

export abstract class ConjuredItemStrategy extends BaseStrategy implements StrategyInterface {
    public static process(item: Item) {
        this.decreaseSellIn(item);

        this.decreaseQuality(item, 2);

        if (this.isExpired(item)) {
            this.decreaseQuality(item, 2);
        }
    }
}