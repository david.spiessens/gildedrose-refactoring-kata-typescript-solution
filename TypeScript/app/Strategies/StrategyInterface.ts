import { Item } from "../gilded-rose";

export interface StrategyInterface {
    process: (item: Item) => void;
}