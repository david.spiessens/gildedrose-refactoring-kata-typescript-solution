import { Item } from "../gilded-rose";
import { BaseStrategy } from "./BaseStrategy";
import { StrategyInterface } from "./StrategyInterface";

export abstract class LegendaryItemStrategy extends BaseStrategy implements StrategyInterface {
    public static process(item: Item) {
        // do nothing
    }
}