import { Item } from "../gilded-rose";
import { BaseStrategy } from "./BaseStrategy";
import { StrategyInterface } from "./StrategyInterface";

export abstract class BackstagePassStrategy extends BaseStrategy implements StrategyInterface {
    public static process(item: Item) {
        this.decreaseSellIn(item);
        
        if (this.isExpired(item)) {
            item.quality = this.minimumQuality;
            
            return;
        }

        this.increaseQuality(item);
        
        if (item.sellIn < 11) {
                this.increaseQuality(item);
        }
        if (item.sellIn < 6) {
                this.increaseQuality(item);
        }
    }
}