import { Item } from "../gilded-rose";
import { StrategyInterface } from "./StrategyInterface";

export abstract class BaseStrategy implements StrategyInterface{
    protected static readonly minimumQuality = 0;
    protected static readonly maximumQuality = 50;


    public process(item: Item) {
        // empty
    }

    protected static isExpired(item: Item) {
        return item.sellIn < 0;
    }

    protected static decreaseQuality(item: Item, amount: number = 1) {
        if (item.quality - amount >= this.minimumQuality) {
            item.quality -= amount;
        }
    }

    protected static increaseQuality(item: Item, amount: number = 1) {
        if (item.quality + amount <= this.maximumQuality) {
            item.quality += amount;
        }
    }

    protected static decreaseSellIn(item: Item) {
        item.sellIn -= 1;
    }
}