import { Item } from "../gilded-rose";
import { BaseStrategy } from "./BaseStrategy";
import { StrategyInterface } from "./StrategyInterface";

export abstract class AgedBrieStrategy extends BaseStrategy implements StrategyInterface {
    public static process(item: Item) {
        this.decreaseSellIn(item);
        
        this.increaseQuality(item)

        if (this.isExpired(item)) {
            this.increaseQuality(item)
        }
    }
}